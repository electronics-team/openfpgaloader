openfpgaloader (0.13.1-1) unstable; urgency=medium

  * New upstream version
  * Update d/copyright
  * Autogenerate man page with help2man to eliminate static patch
  * Standards-Version: 4.7.2 (routine-update)

 -- Steffen Moeller <moeller@debian.org>  Sun, 02 Mar 2025 21:31:27 +0100

openfpgaloader (0.12.1-1) unstable; urgency=medium

  * Team upload.

  * New upstream version 0.12.1.
  * Replaced obsolete pkg-config build dependency with pkgconf.
  * Added Appstream metainfo XML (Closes: #1071004).
  * Moved udev rules from /lib/ to /usr/lib/ (Closes: #1073764).
  * Updated Standards-Version from 4.6.2 to 4.7.0. No changes needed.
  * Added text format man page installed using txt2man.

 -- Petter Reinholdtsen <pere@debian.org>  Sun, 25 Aug 2024 07:58:05 +0200

openfpgaloader (0.12.0-1) unstable; urgency=medium

  [ Steffen Moeller ]
  * d/copyright: Added missing license information
    ./src/svf_jtag.cpp: * Copyright (C) 2022 phdussud
    Special thanks go to FTPmaster Thorsten for pointing this out.

  [ Steffen Möller ]
  * New upstream version
  * Remove trailing whitespace in debian/changelog (routine-update)

 -- Steffen Möller <moeller@debian.org>  Sun, 17 Mar 2024 21:21:08 +0100

openfpgaloader (0.10.0+git20230202-edea24f-1) unstable; urgency=medium

  * Initial source upload after package was accepted.
    - adopts upstream's improved handling of compressed firmwares.

 -- Steffen Moeller <moeller@debian.org>  Fri, 03 Feb 2023 17:30:59 +0100

openfpgaloader (0.10.0-3) unstable; urgency=medium

  * Initial release. (Closes: #1018294)
  * d/copyright: Fixed omission of new contributors
  * d/u/metadata: Added SciCrunch RRID

 -- Steffen Moeller <moeller@debian.org>  Sat, 14 Jan 2023 01:38:40 +0100

openfpgaloader (0.10.0-1) UNRELEASED; urgency=medium

  * New upstream version
    - upstream kindly removed non-DFSG-compliant file from source tree
  * Standards-Version: 4.6.2
  * Avoid explicitly specifying -Wl,--as-needed linker flag.

 -- Steffen Moeller <moeller@debian.org>  Thu, 22 Dec 2022 15:16:35 +0100

openfpgaloader (0.9.0-1) UNRELEASED; urgency=medium

  * Initial upload to experimental.

 -- Steffen Moeller <moeller@debian.org>  Sun, 28 Aug 2022 15:38:29 +0200
